package com.nostratech.catalog.service.impl;

import org.springframework.stereotype.Service;

import com.nostratech.catalog.dto.CustomerResponseDTO;
import com.nostratech.catalog.service.GreetingService;

@Service
public class GreetingServiceImpl implements GreetingService {

	@Override
	public CustomerResponseDTO findCustomer() {
		CustomerResponseDTO dto = new CustomerResponseDTO();
		dto.setAddress("Jakarta");
		dto.setName("Joko Widodo");
		return dto;
	}

	

}
