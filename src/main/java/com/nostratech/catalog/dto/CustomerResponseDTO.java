package com.nostratech.catalog.dto;

import java.io.Serializable;

public class CustomerResponseDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8414976467750896252L;

	private String name;
	
	private String address;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	
	
}
