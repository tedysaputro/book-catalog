package com.nostratech.catalog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.nostratech.catalog.dto.BookRequestDTO;
import com.nostratech.catalog.dto.CustomerResponseDTO;

@Controller
public class BookController {

	@GetMapping("/book/new")
	public String getNewBookPage(Model model) {
		
		model.addAttribute("bookRequestDTO", new BookRequestDTO());
		return "book/book-new";
	}
	
	@PostMapping("/book/new")
	public String createNewBook(Model model, @ModelAttribute("bookRequestDTO") BookRequestDTO dto) {
		System.out.println(dto.getTitle());
		System.out.println(dto.getDescription());
		return "redirect:/book/new";
	}
}
