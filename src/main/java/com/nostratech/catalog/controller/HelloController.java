package com.nostratech.catalog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller //special annotation -> Spring MVC (Modul)
public class HelloController {
	
	
	@GetMapping("/hello")
	public String getHello() {
		return "hello";
	}

}
