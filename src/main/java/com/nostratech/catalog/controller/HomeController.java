package com.nostratech.catalog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.nostratech.catalog.dto.CustomerResponseDTO;
import com.nostratech.catalog.service.GreetingService;

@Controller
public class HomeController {

	private final GreetingService greetingService;
	
	
	
	public HomeController(GreetingService greetingService) {
		super();
		this.greetingService = greetingService;
	}



	@GetMapping("/home")
	public String getHome(Model model) {
		CustomerResponseDTO dto = greetingService.findCustomer();
		model.addAttribute("customer", dto);
		return "home";
	}
	
	

}
